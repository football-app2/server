<!DOCTYPE html>
<html>
<head>
<title>Авторизация</title>
 
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--Bootsrap 4 CDN-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{url('style.css')}}">
 
</head>
<body>

<div class="login d-flex align-items-center py-5">
<div class="container">
    <div class="row">
    <div class="col-md-9 col-lg-8 mx-auto">
        <form action="{{url('post-login')}}" method="POST" id="logForm">

            {{ csrf_field() }}

        <div class="form-label-group">
        <label for="inputEmail">Телефон</label>
            <input type="text" name="phone" id="inputEmail" class="form-control" placeholder="Номер телефона" >
            

            @if ($errors->has('phone'))
            <span class="error">{{ $errors->first('phone') }}</span>
            @endif    
        </div> 

        <div class="form-label-group">
        <label for="inputPassword">Пароль</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль">
            
            
            @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
            @endif  
        </div>
        <br>

        <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Войти</button>
        
        </form>
    </div>
    </div>
</div>
</div>
 
</body>
</html>