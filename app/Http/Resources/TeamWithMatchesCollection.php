<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TeamWithMatchesCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $teams = [];
dd($this);
        foreach($this->collection as $team) {
            dd($team->matches);
            array_push($teams, [
                'id' => $team->id,
                'name' => $team->club,
                'city' => $team->city,
                'avatar' => $team->club_img,
                //'matches' => MatchesCollection::collection($team->mathes)
            ]);
        }

        return $teams;
    }
}
