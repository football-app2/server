<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('login');
    }
    public function postLogin()
    {
        $request = request()->validate([
            'phone' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt(array_merge($request, ['id' => 1]))) {
            // Authentication passed...
            return redirect('/admin');
        }
        return redirect("/");
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return redirect('/');
    }

    public function doPayment()
    {
        $array = [
            'Amount' => $order['amount'], // Required
            'Currency' => 'USD', // Required
            'Name' => $order['firstname'], // Required
            'IpAddress' => getHostByName(getHostName()), // Required
            'CardCryptogramPacket' => $CardCryptogramPacket, // Required
            'InvoiceId' => $order['orderId'],
            'Description' => 'Payment for order №' . $order['orderId'],
            'AccountId' => '999',
            'Email' => $order['email'],
            'JsonData' => json_encode([
                'middleName' => $order['lastname'],
                'lastName' => $order['surname'],
                'phone' => $order['phone'],
            ]),
        ];

        // Trying to do Payment
        try {
            $result = CloudPayments::class;
        } catch (\Exception $e) {
            $result = $e->getMessage();
        }
    }
}
