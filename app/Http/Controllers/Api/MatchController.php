<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Match;
use Jenssegers\Date\Date;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function round(Request $request)
    {
        //
        $validData = $request->validate([
            'from' => 'integer',
            'to' => 'integer',
            'champType' => 'integer|required',
        ]);

        $matchs = Match::with('matchTeams', 'matchTeams.team');
        $matchs = $matchs->where('champ_id', $validData['champType']);
         if (isset($validData['from'])) {
            $matchs = $matchs->where('date', '>=', $validData['from']);
        } else if (isset($validData['from'])) {
            $matchs = $matchs->where('date', '<=', $validData['to']);
        }
        $matchs = $matchs->orderBy('round', 'desc')->get();

        $r = [];
        foreach($matchs as $v) {
            $teams = [];
            $score = [];
            $date = Date::parse($v->date)->format('j M');

            foreach($v->matchTeams as $team) {
                $teams[] = [
                    'id' => $team->team->id,
                    'name' => $team->team->name,
                    'avatar' => $team->team->avatar,
                ];
                $score[] = $team->goal;
            }

            if (!isset($r[$v->round])) $r[$v->round] = ['round' => $v->round, 'matches' => []];
            $r[$v->round]['matches'][] = [
                'teams' => $teams,
                'date' => $date,
                'score' => $score,
            ];
        }

        return response()->json(array_values($r), 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tour(Request $request)
    {
        //
        $validData = $request->validate([
            'from' => 'integer',
            'to' => 'integer',
            'champType' => 'integer|required',
        ]);

        $matchs = Match::with('matchTeams', 'matchTeams.team', 'stadium');
        $matchs = $matchs->where('champ_id', $validData['champType']);
         if (isset($validData['from'])) {
            $matchs = $matchs->where('date', '>=', $validData['from']);
        } else if (isset($validData['from'])) {
            $matchs = $matchs->where('date', '<=', $validData['to']);
        }
        $matchs = $matchs->orderBy('tour', 'desc')->paginate(7);

        $r = [];
        foreach($matchs as $v) {
            $teams = [];
            $date = Date::parse($v->date)->format('j M');

            foreach($v->matchTeams as $team) {
                $teams[] = [
                    'id' => $team->team->id,
                    'name' => $team->team->name,
                    'avatar' => $team->team->avatar,
                ];
            }
            if (!isset($r[$v->tour])) $r[$v->tour] = ['tour' => $v->tour, 'matches' => []];
            $r[$v->tour]['matches'][] = [
                'teams' => $teams,
                'date' => $date,
                'stadium' => $v->stadium
            ];
        }

        return response()->json(array_values($r), 200);
    }
}
