<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\User;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function webhook(Request $request)
    {
        $data = $request->all();
        $payment = new Payment();
        $payment->order_id = $request->TransactionId;
        $payment->amount = $request->Amount;
        $payment->status = $request->Status;
        $payment->phone = $request->AccountId;
        $payment->title = $request->Description;
        $payment->raw = json_encode($data, true);
        $payment->save();

        $user = User::query()->where('phone',$request->AccountId)->first();
        $user->active = true;
        $user->update();
    }
}
