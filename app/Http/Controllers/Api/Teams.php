<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use App\Models\Category as CategoryAlias;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;

/**
 * Class Teams
 *
 * @property \App\Models\Team $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Teams extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->title = 'Команды';
        $this->addToNavigation()->setPriority(5)->setIcon('fas fa-object-group');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')->setHtmlAttribute('class', 'text-center')->setSearchable('false'),
            AdminColumn::text('name', 'Название'),
            AdminColumn::text('category.name', 'Категория')->setSearchable('false'),
            AdminColumn::text('city', 'Город'),
            AdminColumn::image('avatar', 'Аватар'),

        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setFilters(
                AdminDisplayFilter::field('category_id')->setTitle('Category ID [:value]')
            )
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(false)
            ->paginate(25)
            ->with('category')
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        $display->setColumnFilters([
            null,
            null,
            AdminColumnFilter::select(new CategoryAlias(), 'name')->setDisplay('name')->setPlaceholder('Категория')->setColumnName('category_id'),
            null,
            null
        ]);

        $display->getColumnFilters()->setPlacement('card.heading');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     * @throws \SleepingOwl\Admin\Exceptions\Form\Element\SelectException
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::text('name', 'Название')->required(),
            AdminFormElement::text('city', 'Город')->required(),
            AdminFormElement::select('category_id', 'Категория')->setModelForOptions('\App\Models\Category')->setDisplay('name')->required(),
            AdminFormElement::image('avatar', 'Аватар')->required(),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
