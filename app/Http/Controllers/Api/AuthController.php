<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $rules = [
            'phone' => 'required|min:11',
            'password' => 'required|min:6',
        ];
        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages(),
            ]);

        }
        $user = new User([
            'phone' => $request->phone,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'user' => $user->phone,
            'status' => $user->active,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
//        return response()->json([
//            'message' => 'Successfully created user!'
//        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $rules = [
            'phone' => 'required|min:11',
            'password' => 'required|min:6'
        ];
        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // Validation failed
            return response()->json([
                'error' => $validator->messages(),
            ]);

        }
        $credentials = request(['phone', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Oops! Username or Password is incorrect.'
            ]);
        $user = $request->user();
        //if($user->id === 1){
            $tokenResult = $user->createToken('Personal Access Token');

            $token = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'user' => $user->phone,
                'status' => $user->active,
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]);
        //}
        return response()->json([
            'message' => "Oops! You don't have admin access"
        ]);


    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        $user = $request->user();
        $token = $user->createToken('Personal Access Token');

        return response()->json([
            'user' => $user->phone,
            'status' => $user->active,
            'access_token' => $token->accessToken,
            'token_type' => 'Bearer',
        ]);
    }
}
