<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TeamComposition;
use Illuminate\Http\Request;

class TeamStructure extends Controller
{
    public function getTeamStr()
    {
        $getCommandStructure = TeamComposition::with('get_all_clubs')->orderBy('id', 'desc')->get();

        return response()->json([
            'command_structures' => $getCommandStructure
        ], 201);
    }

    public function addNewPlayer(Request $request)
    {
        $res = json_decode($request->newPlayer);
        TeamComposition::create([
            'club_id' => $res->club,
            'player_name' => $res->name,
            'player_number' => $res->number,
            'birthday' => $res->birthday,
        ]);

        return $this->getTeamStr();
    }

    public function deleteTeamStruc($id)
    {
        $teamSt = TeamComposition::find($id);
        $teamSt->delete();

        return $this->getTeamStr();
    }

    public function getOneStTeam($id)
    {
        $getOneStT = TeamComposition::where('id', $id)->first();
        return response()->json([
            'getNewRes' => $getOneStT
        ], 201);
    }

    public function editPlayerSt(Request $request)
    {
        $playerRes = json_decode($request->editPlayer);
        TeamComposition::where('id', $playerRes->id)->update([
            'club_id' => $playerRes->club_id,
            'player_name' => $playerRes->name,
            'player_number' => $playerRes->number,
            'birthday' => $playerRes->birthday,
        ]);

        return $this->getTeamStr();
    }
}
