<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;

/**
 * Class Result
 *
 * @property \App\Models\ChampTeam $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Result extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->title = 'Результаты';
        $this->addToNavigation()->setPriority(2)->setIcon('fas fa-futbol');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('champ.name', 'Чемпионат'),
            AdminColumn::text('team.name', 'Команда'),
            AdminColumn::text('z', 'Забито'),
            AdminColumn::text('p', 'Пропустили'),
            AdminColumn::text('o', 'Очки'),
            AdminColumn::text('year', 'Год'),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->setDisplaySearch(false)
            ->paginate(25)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::select('champ_id', 'Чемпионат', \App\Models\Champ::class)
                ->setDisplay('name')->required(),
            AdminFormElement::select('team_id', 'Команда', \App\Models\Team::class)
                ->setDisplay('name')->required(),
            AdminFormElement::text('z', 'Забили')->required(),
            AdminFormElement::text('p', 'Пропустили')->required(),
            AdminFormElement::text('o', 'Очки')->required(),
            AdminFormElement::text('year', 'Год')->required(),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
