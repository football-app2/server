<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $champ_id
 * @property integer $team_id
 * @property integer $z
 * @property integer $p
 * @property integer $o
 * @property integer $year
 * @property string $created_at
 * @property string $updated_at
 * @property Champ $champ
 * @property Team $team
 */
class ChampTeam extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['champ_id', 'team_id', 'z', 'p', 'o', 'year'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function champ()
    {
        return $this->belongsTo('App\Models\Champ');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }
}
