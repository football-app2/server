<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamComposition extends Model
{
    protected $fillable = [
      'club_id', 'player_name', 'player_number', 'birthday'
    ];

    public function get_all_clubs() {
        return $this->belongsTo(TeamList::class,'club_id','id');
    }
}
