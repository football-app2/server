<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $match_id
 * @property integer $team_id
 * @property boolean $goal
 * @property string $created_at
 * @property string $updated_at
 * @property Match $match
 * @property Team $team
 */
class MatchTeam extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    
    /**
     * @var array
     */
    protected $fillable = ['match_id', 'team_id', 'goal'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match()
    {
        return $this->belongsTo('App\Models\Match');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }
}
