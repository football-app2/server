<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property ChampTeam[] $champTeams
 * @property Match[] $matches
 * @property Result[] $results
 * @property Trainer[] $trainers
 */
class Category extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    protected $isSearchable = false;

    public function teams()
    {
        return $this->hasMany(Team::class,'category_id','id');
    }
}
